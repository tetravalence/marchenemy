<?php

/* 
 * Copyright (C) 2017 docwho
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


function get_cover() {

	echo<<<COVER
<img src="images/schemeback.jpg">
COVER
. PHP_EOL;

}

function get_footer() {
	echo<<<FOOTER
</article>
FOOTER
. PHP_EOL;

	print_debug();
	echo<<<FOOTER
<footer role="copyright">
MAGIC: THE GATHERING® is a trademark[s] of Wizards of the Coast. For more information about Wizards of the Coast or any of Wizards' trademarks or other intellectual property, please visit their website at (<a href="http://archive.wizards.com">archive.wizards.com</a>)”	
</footer>	
</body>
</html>	
FOOTER;

}

function get_head() {
	
	global $title;

	if ( !isset( $title ) ) {
		$title = 'Magic Archenemy';
	}

	echo<<<HEADER
<!DOCTYPE html>
<html>
  <head>
	<title>$title</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/mobile.css" type="text/css" media="screen and (max-width:800px)" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen and (min-width:801px)" />

  </head>
	<body>
HEADER
. PHP_EOL;

}

function get_header( $selected = 0, $action = '#' ) {
	
	global $header;
	$choices = array (
		1 => 'atdm_decklist', 
		2 => 'batua_decklist',
		3 => 'stwwd_decklist',
		4 => 'tcu_decklist',
	);
	$values = array (
		1 => 'ASSEMBLE THE DOOMSDAY MACHINE',
		2 => 'BRING ABOUT THE UNDEAD APOCALYPSE',
		3 => 'SCORCH THE WORLD WITH DRAGONFIRE',
		4 => 'TRAMPLE CIVILIZATION UDERFOOT',
	);
	$options = '<option value="">---</option>' . PHP_EOL;
	$i = 1;
	
	if ( !isset( $header ) ) {
		$header = '<h1>My Archenemy</h1>';
	}
	
	foreach ( $choices as $choice => $value ) {
		if ( $choice == $selected ) {
			$options .= '<option value="' . "$value" . '" selected>' . $values[$i] . '</option>'. PHP_EOL;
		} else {
			$options .= '<option value="' . "$value" . '">' . $values[$i] . '</option>' . PHP_EOL;
		}
		$i++;
	}

	echo<<<FORM
<header role="banner">
<form method="post" action="$action">
$header
<label>Select a deck</label>
<div class="select-wrapper">
	<select id="deck-select" name="deck_select">
	$options
	</select>
</div>
<nav role="navigation">
<input type="submit" name="action" value="SHUFFLE">
<input type="submit" name="action" value="DECKLIST">
<input type="submit" name="action" value="PLAY">
</nav>
</form>
</header>
<article>
<p>Paragrafo 1</p>
FORM
. PHP_EOL;

}

function print_deck( $deck = '' ) {

	$decklist = get_decklist( $deck );
	
	foreach ( $decklist as $key => $value ) {
		echo "<p>$value[0]. $key</p>", PHP_EOL;
	}	
}

function print_debug() {
	global $msg;
	
	if ( empty( $msg ) ) {
		return;
	} 
	
	echo<<<DEBUG
<article>
DEBUG
. PHP_EOL;
	
	foreach ( $msg as $txt ) {
		echo '<pre>', $txt, '</pre>', PHP_EOL;
	}
	
	echo<<<DEBUG
</article>
DEBUG
. PHP_EOL;
}