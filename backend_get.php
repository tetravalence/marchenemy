<?php

/* 
 * Copyright (C) 2017 docwho
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function method_get() {
	$action = htmlentities( $_SERVER['SCRIPT_NAME'] );
	/*
	 * Use of the various filter functions ensure you treat user input with proper
	 * validation and sanitization.
   * If there is a valid query string then play function displays the card. 
   */
  if ( filter_has_var( INPUT_GET, 'card' ) &&
		filter_has_var( INPUT_GET, 'select' ) ) {
		$card = filter_input( INPUT_GET, 'card', FILTER_SANITIZE_STRING );
		$select = filter_input( INPUT_GET, 'select', FILTER_SANITIZE_STRING );
		$selected_deck = get_selected_deck( $select );
		get_header( $selected_deck, $action );
		play( $select, $card );
	} else {
		//get_header( array (), $action );
		get_header( 0, $action );
		get_cover();
	}

}
