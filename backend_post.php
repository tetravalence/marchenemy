<?php

/* 
 * Copyright (C) 2017 docwho
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function method_post( ) {
	global $msg;
	
	$choices = array (
		'atdm_decklist',
		'batua_decklist',
		'stwwd_decklist',
		'tcu_decklist',
	);
	$input_deck = filter_input( INPUT_POST, 'deck_select', FILTER_SANITIZE_STRING );

	if ( ( $input_deck === '' ) || ( $input_deck == false ) ) {
		get_header();
		get_cover();
		$msg[] = 'You have to select a deck';
		return;
	}
	
	if ( !in_array( $input_deck, $choices ) ) {
		get_header();
		get_cover();
		$msg[] = 'The selected deck does not exist';
		return;
	}
/*
	switch ( $input_deck ) {
		case 'atdm_decklist':
		case 'batua_decklist':
		case 'stwwd_decklist':
		case 'tcu_decklist':
			break;
		default:
			get_header();
			get_cover();
			return;
	} */
	
	$action = filter_input( INPUT_POST, 'action', FILTER_SANITIZE_STRING );

	if ( ( $action === '' ) || ( $action == false ) ) {
		get_header();
		return;
	}

	$selected_deck = get_selected_deck( $input_deck );
	get_header( $selected_deck );

	switch ( $action ) {
		case 'SHUFFLE':
			setup_deck( $input_deck );
			get_cover();
			break;
		case 'DECKLIST':
			print_deck( $input_deck );
			break;
		case 'PLAY':
			first_card( $input_deck );
			break;
		default:
			get_cover();
	}

}
